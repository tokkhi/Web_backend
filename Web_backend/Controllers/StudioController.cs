﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_backend.Models;
using static Web_backend.Models.Show;

namespace Web_backend.Controllers
{
    public class StudioController : Controller
    {
        // GET: Studio
        public ActionResult Index()
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            Studio studio = new Studio();
            DataClass.TB_Movies dbmovie = new DataClass.TB_Movies();
            studio.studio = new List<List_Studio>();
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            {
                studio.studio = db.TB_Studios.Select(d => new List_Studio { ID = d.ID_Studio, Name = d.Name_Studio }).ToList();
                return View(studio.studio);
            }
        }
        public ActionResult Add()
        {
            return View();
        }
        public ActionResult Edit(string id)
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            var model = new Edit.Input_Studio();
          
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            {
                var Edit = db.TB_Studios.Where(m => m.ID_Studio == id).FirstOrDefault();
                if (Edit != null)
                {
                    model.ID = Edit.ID_Studio;
                    model.Name = Edit.Name_Studio;
                    model.Url = Edit.Link_Studio;
                }
                return View(model);
            }
         
        }
    }
}