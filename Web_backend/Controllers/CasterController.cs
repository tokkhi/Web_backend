﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Web_backend.Helper;
using Web_backend.Models;

namespace Web_backend.Controllers
{
    public class CasterController : Controller
    {
        // GET: Caster
        public ActionResult Index()
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            Caster caster = new Caster();
            DataClass.TB_Movies dbmovie = new DataClass.TB_Movies();
            caster.caster = new List<List_Caster>();
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            {
                var result = (from c in db.TB_Casters select c);

                foreach (var item in result.AsParallel())
                {
                    caster.caster.Add(new List_Caster
                    {
                        ID_Caster = item.ID_Caster,
                        Detail = item.Detail_Caster,
                        Name = item.Name_Lastname_Caster
                    });
                }
            }
            return View(caster.caster);
        }public ActionResult Add(){
            var model = new Add.Input_caster();
            return View(model);
        }
        public ActionResult Edit(string id)
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            var model = new Edit.Input_Caster();
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            {
                var caster = db.TB_Casters.Where(m => m.ID_Caster == id).FirstOrDefault();
                model.ID_caster = caster.ID_Caster;
                model.Name = caster.Name_Lastname_Caster;
                model.Detail = caster.Detail_Caster;
                string url = WebConfigurationManager.AppSettings["Path_img"];
                model.PictureUrl = url + caster.Picture_Caster;
                model.BrithDay = GetValue.DateToString(caster.Brithday_Caster); 
                ViewBag.name_img = caster.Picture_Caster;

            }
            return View(model);
            
        }
        private void add_DD()
        {
            

        }
    }
}