﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_backend.Models;
using static Web_backend.Models.Show;

namespace Web_backend.Controllers
{
    public class GenreController : Controller
    {
        // GET: Genre
        public ActionResult Index()
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            Genre genre = new Genre();
            DataClass.TB_Movies dbmovie = new DataClass.TB_Movies();
            genre.genre = new List<List_Genre>();
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            {
                genre.genre = db.TB_Genres.Select(d => new List_Genre { ID = d.ID_Genre, Name = d.Name_Genre }).ToList();
                return View(genre.genre);
            }

        }
        public ActionResult Add()
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            return View();
        }
        public ActionResult Edit(string id)
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            var model = new Edit.Input_Genre();
            Edit_Movie movie = new Edit_Movie();
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            {
                var Edit = db.TB_Genres.Where(m => m.ID_Genre == id).FirstOrDefault();
                if (Edit != null)
                {
                    model.ID = Edit.ID_Genre;
                    model.Name = Edit.Name_Genre;
                }
                return View(model);
            }
        }
    }
}