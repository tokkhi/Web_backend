﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Web_backend.Models.Delete;

namespace Web_backend.Controllers
{
    public class DeleteController : Controller
    {
        private string APIURL = string.Empty;
        // GET: Delete
        public ActionResult Director(Input_Director input)
        {
            Output output_ = new Output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Delete/Director", input);
            output_ = Newtonsoft.Json.JsonConvert.DeserializeObject<Output>(result);

            return RedirectToAction("Index", "Director");
        }
        public ActionResult Movie(Input_Movie input)
        {
            Output output_ = new Output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Delete/Movie", input);
            output_ = Newtonsoft.Json.JsonConvert.DeserializeObject<Output>(result);

            return RedirectToAction("Index", "Movie");
        }
        public ActionResult Caster(Input_Caster input)
        {
            Output output_ = new Output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Delete/Caster", input);
            output_ = Newtonsoft.Json.JsonConvert.DeserializeObject<Output>(result);

            return RedirectToAction("Index", "Caster");
        }
        public ActionResult Genre(Input_Genre input)
        {
            Output output_ = new Output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Delete/Genre", input);
            output_ = Newtonsoft.Json.JsonConvert.DeserializeObject<Output>(result);

            return RedirectToAction("Index", "Genre");
        }
        public ActionResult Studio(Input_Studio input)
        {
            Output output_ = new Output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Delete/Studio", input);
            output_ = Newtonsoft.Json.JsonConvert.DeserializeObject<Output>(result);

            return RedirectToAction("Index", "Studio");
        }
        public ActionResult User(Input_User input)
        {
            Output output_ = new Output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Delete/User", input);
            output_ = Newtonsoft.Json.JsonConvert.DeserializeObject<Output>(result);

            return RedirectToAction("Index", "Users");
        }
        public ActionResult News(Input_News input)
        {
            Output output_ = new Output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Delete/News", input);
            output_ = Newtonsoft.Json.JsonConvert.DeserializeObject<Output>(result);

            return RedirectToAction("Index", "News");
        }
        public ActionResult Comment(Input_Comment input)
        {
            Output output_ = new Output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Review/Delete_Comment", input);
            output_ = Newtonsoft.Json.JsonConvert.DeserializeObject<Output>(result);

            return RedirectToAction("Index", "Comment");
        }
    }

}