﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_backend.Models;

namespace Web_backend.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        public ActionResult Index()
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            Users.output user = new Users.output();
            DataClass.TB_Movies dbmovie = new DataClass.TB_Movies();
            user.user = new List<List_User>();
            using (DataClass.UsersDataContext db = new DataClass.UsersDataContext())
            {
                var result = (from u in db.TB_Users join t in db.TB_Type_Users on u.ID_Type equals t.ID_Type select new { u, t });

                foreach (var item in result.AsParallel())
                {
                    user.user.Add(new List_User
                    {
                        Guid_User = item.u.Guid_Users,
                        Email = item.u.Email,
                        Name = item.u.Name_Lastname_Users,
                        type = item.t.Name_Type
                    });
                }
            }
            return View(user.user);
        }
        public ActionResult Edit(string id)
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            var model = new Edit.Input_User();
            add_DD();
            using (DataClass.UsersDataContext db = new DataClass.UsersDataContext())
            {
                var Edit = db.TB_Users.Where(m => m.Guid_Users.ToString() == id).FirstOrDefault();
                if (Edit != null)
                {
                    model.guid = Edit.Guid_Users;
                    model.Email = Edit.Email;
                    model.Name = Edit.Name_Lastname_Users;
                    model.Password = Edit.Password;
                    model.mtype = Edit.ID_Type;
                }
                return View(model);
            }
        }
        private void add_DD()
        {
            
           
            using (DataClass.UsersDataContext db = new DataClass.UsersDataContext())
            {
                Edit_User user = new Edit_User();
                user.Type = new List<list_DD>();
                var Type = (from c in db.TB_Type_Users select c);
                foreach (var item in Type.AsParallel())
                {
                    user.Type.Add(new list_DD
                    {
                        id = item.ID_Type,
                        Name = item.Name_Type
                    });
                }
                ViewBag.Type = user.Type;
            }
        }
    }
}