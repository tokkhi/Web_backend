﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_backend.Models;
using static Web_backend.Models.Show;

namespace Web_backend.Controllers
{
    public class NewsController : Controller
    {
        // GET: News
        public ActionResult Index()
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            News news = new News();

            news.news = new List<List_News>();
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            {
                news.news = db.TB_News.Select(d => new List_News { ID = d.ID_News.ToString(), Name = d.Name_News }).ToList();
                return View(news.news);
            }
            
        }
        public ActionResult Add()
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            News news = new News();

                return View(news.news);
            

        }
        public ActionResult Edit(int id)
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }

            var model = new Edit.Input_News();
            
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            {
                var Edit = db.TB_News.Where(m => m.ID_News == id).FirstOrDefault();
                if (Edit != null)
                {
                    model.ID = Edit.ID_News.ToString();
                    model.Name = Edit.Name_News;
                    model.Url = Edit.Link_News;
                }
                return View(model);
            }

        }
    }
}