﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_backend.Models;

namespace Web_backend.Controllers
{
    public class UpdateController : Controller
    {
        private string APIURL = string.Empty;
        // GET: Update
        public ActionResult Movie(Edit.Input_movie input, HttpPostedFileBase file)
        {
            var guid = Session["Guid"] as string;
            if (file != null)
            {
                var returnedClass = new Helper.Upload();
                input.img = returnedClass.Image(file, Server.MapPath(ConfigurationManager.AppSettings["Path"]));
            }
            input.ID_User = new Guid(guid);
            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Edit/Movie", input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Index","Movie");
        }
        public ActionResult Caster(Edit.Input_Caster Input, HttpPostedFileBase file)
        {
            if (file != null)
            {
                var returnedClass = new Helper.Upload();
                Input.PictureUrl = returnedClass.Image(file, Server.MapPath(ConfigurationManager.AppSettings["Path"]));
            }


            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Edit/Caster", Input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Index", "Caster");
        }
        public ActionResult Director(Edit.Input_director Input, HttpPostedFileBase file)
        {
            if (file != null)
            {
                var returnedClass = new Helper.Upload();
                Input.PictureUrl = returnedClass.Image(file, Server.MapPath(ConfigurationManager.AppSettings["Path"]));
            }


            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Edit/Director", Input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Index", "Director");
        }
        public ActionResult Genre(Edit.Input_Genre Input)
        {

            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Edit/Genre", Input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Index", "Genre");
        }
        public ActionResult Studio(Edit.Input_Studio Input)
        {

            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Edit/Studio", Input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Index", "Studio");
        }
        public ActionResult News(Edit.Input_News Input)
        {

            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Edit/News", Input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Index", "News");
        }
        public ActionResult User(Edit.Input_User Input)
        {
            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL +"/Register/Edit", Input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Index", "Users");
        }
        public ActionResult Status(Edit.Input_Status Input)
        {
            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Edit/Status_Movie", Input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Index", "Home");
        }
}
}