﻿using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Mvc;
using Web_backend.Models;

namespace Web_backend.Controllers
{
    public class LoginController : Controller
    {
        private string APIURL =  string.Empty;
      
        public ActionResult Index()
        {
            if (Session["Guid"] != null)
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        public ActionResult Users()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginTest(Login input)
        {
            output output = new output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Login/Admin", input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            if (output.return_message == "Success")
            {
               var myCookie = new HttpCookie("User");
                myCookie.Value = output.token.Guid;
                myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
                Session["Guid"] = output.token.Guid;
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index","Login");
        }
        [HttpPost]
        public ActionResult Login_User(Login input)
        {
            output output = new output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Login", input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            if (output.return_message == "Success")
            {
                Session["Guid"] = output.token.Guid;
                return RedirectToAction("Add", "Movie");
            }

            return RedirectToAction("Users", "Login");
        }

    }
}