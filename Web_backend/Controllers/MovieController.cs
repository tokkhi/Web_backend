﻿using System;
using System.Collections.Generic;
using System.Linq;
using Web_backend.Models;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using Web_backend.Helper;
using System.Web.Configuration;

namespace Web_backend.Controllers
{
    public class MovieController : Controller
    {
        private string APIURL = string.Empty;
        // GET: Movie
        public ActionResult Index()
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            Movie movie = new Movie();
            DataClass.TB_Movies dbmovie = new DataClass.TB_Movies();
            movie.movie = new List<List_Movie>();
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())

            {
                var result = (from c in db.TB_Movies select c);

                foreach (var item in result.AsParallel())
                {
                    movie.movie.Add(new List_Movie
                    {
                        ID_Movie = item.ID_Movie,
                        date = item.Date_Movie.Value.ToString(),
                        Name = item.Name_Movie,
                        status = item.show.Value

                    });
                }
            }
            return View(movie.movie);
        }
        public ActionResult Edit(string id)
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            var model = new Edit.Input_movie();
            add_DD();
            Edit_Movie movie = new Edit_Movie();
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            {
                try
                {
                    var Edit = db.TB_Movies.Where(m => m.ID_Movie == id).FirstOrDefault();
                    if (Edit != null)
                    {

                        model.ID_Movie = Edit.ID_Movie;
                        model.Name_Movie = Edit.Name_Movie;
                        model.Name_Eng = Edit.Name_Movie_eng;
                        model.urlTri = Edit.Trialer;
                        if (Edit.Date_Movie.HasValue)
                        {
                            model.Date_Movie = GetValue.DateToString(Edit.Date_Movie);
                        }
                        model.Detail = Edit.Detail_Movie;
                        model.Rate = Edit.ID_Rate;
                        string url = WebConfigurationManager.AppSettings["Path_img"];
                        model.studio = Edit.ID_Studio;
                        model.img = url + Edit.Picture_Movie;
                        ViewBag.name_img = Edit.Picture_Movie;
                    }
                    var Caster_Movie = db.TB_Cast_Movies.Where(c => c.ID_Movie == id);
                    if (Caster_Movie != null)
                    {
                        foreach (var item in Caster_Movie)
                        {
                            model.Caster.Add(new Models.Edit.List_Cs_Mo
                            {
                                id_Cs = item.ID_Caster,
                                status = item.ID_Status_Caster.Value

                            });
                        }
                    }

                    var Director_Movie = db.TB_Director_Movies.Where(c => c.ID_Movie == id);
                    if (Director_Movie != null)
                    {
                        foreach (var item in Director_Movie)
                        {
                            model.Director.Add(new Models.Edit.List_Dt_Mo
                            {
                                id_Dt = item.ID_Director,
                                status = item.ID_Status_Director.Value

                            });
                        }
                    }

                    var Genre_Movie = db.TB_Genre_Movies.Where(c => c.ID_Movie == id);
                    if (Genre_Movie != null)
                    {
                        foreach (var item in Genre_Movie)
                        {
                            model.Genre.Add(new Models.Edit.List_Ge_Mo
                            {
                                id_Ge = item.ID_Genre

                            });
                        }
                    }


                }
                catch (Exception ex)
                {

                }


                return View(model);
            }
        }
        public ActionResult Delete(string id)
        {
            

            return View();
        }
        public ActionResult Add()
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            var model = new Add.Input_movie();

           // model.ID_User = new Guid(Session["Guid"] as string);
            add_DD();
            return View(model);
        }
        public ActionResult Add_M()
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            var model = new Add.Input_movie();

            // model.ID_User = new Guid(Session["Guid"] as string);
            add_DD();
            return View(model);
        }
        public ActionResult Edit_M(string id)
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            var model = new Edit.Input_movie();
            add_DD();
            Edit_Movie movie = new Edit_Movie();
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            {
                try
                {
                    var Edit = db.TB_Movies.Where(m => m.ID_Movie == id).FirstOrDefault();
                    if (Edit != null)
                    {

                        model.ID_Movie = Edit.ID_Movie;
                        model.Name_Movie = Edit.Name_Movie;
                        model.urlTri = Edit.Trialer;
                        if (Edit.Date_Movie.HasValue)
                        {
                            model.Date_Movie = GetValue.DateToString(Edit.Date_Movie);
                        }
                        model.Detail = Edit.Detail_Movie;
                        model.Rate = Edit.ID_Rate;
                        model.studio = Edit.ID_Studio;
                        string url = WebConfigurationManager.AppSettings["Path_img"];
                        model.img = url+Edit.Picture_Movie;
                    }
                    var Caster_Movie = db.TB_Cast_Movies.Where(c => c.ID_Movie == id);
                    if (Caster_Movie != null)
                    {
                        foreach (var item in Caster_Movie)
                        {
                            model.Caster.Add(new Models.Edit.List_Cs_Mo
                            {
                                id_Cs = item.ID_Caster,
                                status = item.ID_Status_Caster.Value

                            });
                        }
                    }

                    var Director_Movie = db.TB_Director_Movies.Where(c => c.ID_Movie == id);
                    if (Director_Movie != null)
                    {
                        foreach (var item in Director_Movie)
                        {
                            model.Director.Add(new Models.Edit.List_Dt_Mo
                            {
                                id_Dt = item.ID_Director,
                                status = item.ID_Status_Director.Value

                            });
                        }
                    }

                    var Genre_Movie = db.TB_Genre_Movies.Where(c => c.ID_Movie == id);
                    if (Genre_Movie != null)
                    {
                        foreach (var item in Genre_Movie)
                        {
                            model.Genre.Add(new Models.Edit.List_Ge_Mo
                            {
                                id_Ge = item.ID_Genre

                            });
                        }
                    }


                }
                catch (Exception ex)
                {

                }


                return View(model);
            }
        }

        public ActionResult Detail(string id)
        {
            if (Request.Cookies["User"] == null || Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            var model = new Edit.Input_movie();
            add_DD();
            Edit_Movie movie = new Edit_Movie();
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            {
                try
                {
                    var Edit = db.TB_Movies.Where(m => m.ID_Movie == id).FirstOrDefault();
                    if (Edit != null)
                    {

                        model.ID_Movie = Edit.ID_Movie;
                        model.Name_Movie = Edit.Name_Movie;
                        model.urlTri = Edit.Trialer;
                        if (Edit.Date_Movie.HasValue)
                        {
                            model.Date_Movie = GetValue.DateToString(Edit.Date_Movie);
                        }
                        model.Detail = Edit.Detail_Movie;
                        model.Rate = Edit.ID_Rate;
                        model.studio = Edit.ID_Studio;
                        model.img = Edit.Picture_Movie;
                    }
                    var Caster_Movie = db.TB_Cast_Movies.Where(c => c.ID_Movie == id);
                    if (Caster_Movie != null)
                    {
                        foreach (var item in Caster_Movie)
                        {
                            model.Caster.Add(new Models.Edit.List_Cs_Mo
                            {
                                id_Cs = item.ID_Caster,
                                status = item.ID_Status_Caster.Value

                            });
                        }
                    }

                    var Director_Movie = db.TB_Director_Movies.Where(c => c.ID_Movie == id);
                    if (Director_Movie != null)
                    {
                        foreach (var item in Director_Movie)
                        {
                            model.Director.Add(new Models.Edit.List_Dt_Mo
                            {
                                id_Dt = item.ID_Director,
                                status = item.ID_Status_Director.Value

                            });
                        }
                    }

                    var Genre_Movie = db.TB_Genre_Movies.Where(c => c.ID_Movie == id);
                    if (Genre_Movie != null)
                    {
                        foreach (var item in Genre_Movie)
                        {
                            model.Genre.Add(new Models.Edit.List_Ge_Mo
                            {
                                id_Ge = item.ID_Genre

                            });
                        }
                    }


                }
                catch (Exception ex)
                {

                }


                return View(model);
            }
        }

        private void add_DD()
        {
            Add_Movie movie = new Add_Movie();
            movie.Caster = new List<list_DD>();
            movie.Rate = new List<list_DD>();
            movie.Director = new List<list_DD>();
            movie.Genre = new List<list_DD>();
            movie.Studio = new List<list_DD>();
            movie.S_Caster = new List<list_DD>();
            movie.S_Director = new List<list_DD>();
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            {
                var caster = (from c in db.TB_Casters select c);
                foreach (var item in caster.AsParallel())
                {
                    movie.Caster.Add(new list_DD
                    {
                        id = item.ID_Caster,
                        Name = item.Name_Lastname_Caster
                    });
                }
                ViewBag.Caster = movie.Caster;
                var rate = (from c in db.TB_Rates select c);
                foreach (var item in rate)
                {
                    movie.Rate.Add(new list_DD
                    {
                        id = item.ID_Rate.ToString(),
                        Name = item.Rate
                    });
                }
                ViewBag.Rate = movie.Rate;
                var Directer = (from c in db.TB_Directors select c);
                foreach (var item in Directer)
                {
                    movie.Director.Add(new list_DD
                    {
                        id = item.ID_Director,
                        Name = item.Name_Lastname_Director
                    });
                }
                ViewBag.Director = movie.Director;
                var Genre = (from c in db.TB_Genres select c);
                foreach (var item in Genre)
                {
                    movie.Genre.Add(new list_DD
                    {
                        id = item.ID_Genre,
                        Name = item.Name_Genre
                    });
                }
                ViewBag.Genre = movie.Genre;
                var Studio = (from c in db.TB_Studios select c);
                
                foreach (var item in Studio)
                {
                    movie.Studio.Add(new list_DD
                    {
                        id = item.ID_Studio,
                        Name = item.Name_Studio
                    });
                }
                ViewBag.Studio = movie.Studio;

                var S_Caster = (from c in db.TB_Status_Casters select c);
                foreach (var item in S_Caster)
                {
                    movie.S_Caster.Add(new list_DD
                    {
                        id = item.ID_Status_Caster.ToString(),
                        Name = item.Status_Caster
                    });
                }
                ViewBag.S_Caster = movie.S_Caster;

                var S_Director = (from c in db.TB_Status_Directors select c);
                foreach (var item in S_Director)
                {
                    movie.S_Director.Add(new list_DD
                    {
                        id = item.ID_Status_Director.ToString(),
                        Name = item.Status_Director
                    });
                }
                ViewBag.S_Director = movie.S_Director;
            }

        }
     


    }
}