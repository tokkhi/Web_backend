﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_backend.Models;

namespace Web_backend.Controllers
{
    public class AddController : Controller
    {

        private string APIURL = string.Empty;
        // GET: Add
        public ActionResult Movie(Add.Input_movie Input, HttpPostedFileBase file)
        {
            var guid = Session["Guid"] as string;
            if (file != null)
            {
                var returnedClass = new Helper.Upload();
                Input.img = returnedClass.Image(file,Server.MapPath(ConfigurationManager.AppSettings["Path"]));
            }
            
            Input.ID_User = new Guid(guid);

            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Add/Movie", Input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Index", "Movie");
        }
        public ActionResult Caster(Add.Input_caster Input, HttpPostedFileBase file)
        {
            if (file != null)
            {
                var returnedClass = new Helper.Upload();
                Input.PictureUrl = returnedClass.Image(file, Server.MapPath(ConfigurationManager.AppSettings["Path"]));
            }


            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Add/Caster", Input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Index", "Caster");
        }
        public ActionResult Caster_M(Add.Input_caster Input, HttpPostedFileBase file)
        {
            if (file != null)
            {
                var returnedClass = new Helper.Upload();
                Input.PictureUrl = returnedClass.Image(file, Server.MapPath(ConfigurationManager.AppSettings["Path"]));
            }


            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Add/Caster", Input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Add", "Movie");
        }
        public ActionResult Director(Add.Input_director Input, HttpPostedFileBase file)
        {
            if (file != null)
            {
                var returnedClass = new Helper.Upload();
                Input.PictureUrl = returnedClass.Image(file, Server.MapPath(ConfigurationManager.AppSettings["Path"]));
            }


            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Add/Director", Input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Index", "Director");
        }
        public ActionResult Director_M(Add.Input_director Input, HttpPostedFileBase file)
        {
            if (file != null)
            {
                var returnedClass = new Helper.Upload();
                Input.PictureUrl = returnedClass.Image(file, Server.MapPath(ConfigurationManager.AppSettings["Path"]));
            }


            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Add/Director", Input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Add", "Movie");
        }
        public ActionResult Genre(Add.Input_Genre Input)
        {

            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Add/Genre", Input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Index", "Genre");
        }
        public ActionResult Studio(Add.Input_Studio Input)
        {

            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Add/Studio", Input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Index", "Studio");
        }
        public ActionResult News(Add.Input_News Input)
        {

            Models.output output = new Models.output();
            APIURL = ConfigurationManager.AppSettings["APIURL"];
            Helper.Request api = new Helper.Request();
            var result = api.send(APIURL + "/Add/News", Input);
            output = Newtonsoft.Json.JsonConvert.DeserializeObject<output>(result);
            return RedirectToAction("Index", "News");
        }
    }
}