﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Web_backend.Models;

namespace Web_backend.Controllers
{
    public class HomeController : Controller
    {

      

        // GET: Index
        public ActionResult Index()
        {

            if (Session["Guid"] == null)
            {
                Session.Remove("Guid");
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            Movie movie = new Movie();
            Comment comment = new Comment();
            DataClass.TB_Movies dbmovie = new DataClass.TB_Movies();
            movie.movie = new List<List_Movie>();
            comment.comment = new List<List_Comment>();
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            
            {
                var result = (from c in db.TB_Movies select c).Where(c => c.show == false);

                foreach (var item in result.AsParallel())
                {
                    movie.movie.Add(new List_Movie
                    {
                        ID_Movie = item.ID_Movie,
                        date = item.Date_Movie.Value.ToString(),
                        Name = item.Name_Movie,
                        status = item.show.Value
                    });
                }
                comment.comment = db.Reports.Where(x => x.Count_Report >0).Select(x => new List_Comment { ID = x.ID_Comments.ToString(), Text = x.Comment_Text,Name = x.Name_Movie,Status = x.Count_Report.Value }).ToList();

               
               
            }
            ViewData["Movie"] = movie.movie;
            ViewData["Comment"] = comment.comment;

            return View();
        }
        public ActionResult Logout()
        {

            if (Request.Cookies["User"] != null)
            {
                Session.Remove("Guid");
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
            }

            return RedirectToAction("Index", "Home");
        }
     
    }
}