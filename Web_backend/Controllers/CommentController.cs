﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_backend.Models;

namespace Web_backend.Controllers
{
    public class CommentController : Controller
    {
        // GET: Comment
        public ActionResult Index()
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            Comment comment = new Comment();

            comment.comment = new List<List_Comment>();
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            {
                comment.comment = db.Reports.Select(x => new List_Comment { ID = x.ID_Comments.ToString(), Text = x.Comment_Text, Name = x.Name_Movie, Status = x.Count_Report.Value }).ToList();


            }
                return View(comment.comment);
            }
           
        }
    }