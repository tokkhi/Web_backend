﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Web_backend.Helper;
using Web_backend.Models;

namespace Web_backend.Controllers
{
    public class DirectorController : Controller
    {
        // GET: Directer
        public ActionResult Index()
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            Director Director = new Director();
            DataClass.TB_Movies dbmovie = new DataClass.TB_Movies();
            Director.director = new List<List_Director>();
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            {
                Director.director = db.TB_Directors.Select(d => new List_Director { ID_Directer = d.ID_Director, Name = d.Name_Lastname_Director, Detail = d.Detail_Director }).ToList();
                return View(Director.director);
            }
        }
        public ActionResult Add()
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }

            return View();
            
        }
        public ActionResult Edit(string id)
        {
            if (Session["Guid"] == null)
            {
                Response.Cookies["User"].Expires = DateTime.Now.AddDays(-1);
                return RedirectToAction("Index", "Login");
            }
            var model = new Edit.Input_director();
            using (DataClass.MovieDataContext db = new DataClass.MovieDataContext())
            {
                var director = db.TB_Directors.Where(m => m.ID_Director == id).FirstOrDefault();
                model.id_director = director.ID_Director;
                model.Name = director.Name_Lastname_Director;
                model.Detail = director.Detail_Director;
                string url = WebConfigurationManager.AppSettings["Path_img"];
                model.PictureUrl = url + director.Picture_Director;
                if (director.Brithday_Director != null)
                {
                    model.BrithDay =  GetValue.DateToString(director.Brithday_Director.Value);
                }
                ViewBag.name_img = director.Picture_Director;

            }
            return View(model);
            
        }
    }
}