﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_backend.Helper
{
    public class GetValue
    {
        public static string DateToString(DateTime? Input)
        {

            string output = null;
            if (Input.HasValue)
            {
                output = Input.Value.ToString("MM/dd/yyyy");
            }
            return output;

        }
    }
}