﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Security;

namespace Web_backend.Helper
{
    public class Request
    {
        // GET: Request

        public string send(string url)
        {
            return send(url, null);
        }

        public string send(string url, object input)
        {
            string result = null;
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(input);

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
          

        }
    }
}