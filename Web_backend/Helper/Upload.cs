﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web_backend.Helper
{
    public class Upload
    {
        public string Image(HttpPostedFileBase file,String pathsever)
        {
            if (file.ContentLength > 0)
            {
                var path = (String)null;
                var fileName = (String)null;
                try
                {
                    if (file != null && file.ContentLength > 0 && file.ContentType == "image/jpeg")
                    {
                        fileName = Path.GetFileName(file.FileName);
                        path = Path.Combine(pathsever, fileName);
                        file.SaveAs(path);
                    }
                    else
                    {
                        return "error";
                    }
                }
                catch (Exception ex)
                {
                    Exception ex2 = ex;
                    while (ex2.InnerException != null)
                    {
                        ex2 = ex2.InnerException;
                    }
                    Console.WriteLine(ex.InnerException);
                    throw;
                }

                return fileName;
            }
            return "null";
        }
    }
}