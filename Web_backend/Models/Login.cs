﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_backend.Models
{
    public class Login
    {
            [Required]
            public string Email { get; set; }
            [Required]
            public string password { get; set; }
    }
    public class output
    {
        public string return_message { get; set; }
        public string Name { get; set; }
        public Token token { get; set; }
        public string status  { get; set; }
    }

    public class Token
    {
        public string app_id { get; set; }
        public string Guid { get; set; }
        public string exp { get; set; }
    }
}