﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_backend.Models
{
    public class Caster
    {
        public List<List_Caster> caster { get; set; }
    }
    public class List_Caster
    {
        public string ID_Caster { get; set; }
        public string Name { get; set; }
        public string Detail { get; set; }
    }
}