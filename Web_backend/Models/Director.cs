﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_backend.Models
{
    public class Director
    {
            public List<List_Director> director { get; set; } 
    }
    public class List_Director
    {
        public string ID_Directer { get; set; }
        public string Name { get; set; }
        public string Detail { get; set; }
    }
}