﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_backend.Models
{
    public class Add
    {
        public class Input_movie
        {
            public Input_movie()
            {
                Caster = new List<List_Cs_Mo>();
                Genre = new List<List_Ge_Mo>();
                Director = new List<List_Dt_Mo>();
            }
            public string Name_Movie { get; set; }
            public string Name_Eng { get; set; }
            public DateTime Date_Movie { get; set; }
            public string Detail { get; set; }
            public List<List_Cs_Mo> Caster { get; set; }
            public List<List_Ge_Mo> Genre { get; set; }
            public List<List_Dt_Mo> Director { get; set; }
            public string img { get; set; }
            public string urlTri { get; set; }
            public string studio { get; set; }
            public int Rate { get; set; }
            public Guid ID_User { get; set; }
        }
        public class Input_director
        {
            public string Name { get; set; }
            public DateTime BrithDay { get; set; }
            public string PictureUrl { get; set; }
            public string Detail { get; set; }
        }
        public class Input_Genre
        {
            public string Name { get; set; }
        }
        public class Input_Studio
        {
            public string Name { get; set; }
            public string Url { get; set; }
        }
        public class Input_caster
        {
            public string Name { get; set; }
            public DateTime BrithDay { get; set; }
            public string PictureUrl { get; set; }
            public string Detail { get; set; }
        }
        public class Input_News
        {
            public string ID { get; set; }
            public string Name { get; set; }
            public string Url { get; set; }
        }

    }
    public class List_Cs_Mo
    {
        public string id_Cs { get; set; }
        public string status { get; set; }
    }
    public class List_Ge_Mo
    {
        public string id_Ge { get; set; }   
    }
    public class List_Dt_Mo
    {
        public string id_Dt { get; set; }
        public string status { get; set; }
    }
    
}