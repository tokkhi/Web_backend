﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_backend.Models
{
    public class Comment
    {
        
            public List<List_Comment> comment { get; set; }
        
    }
    public class List_Comment
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public int Status { get; set; }
    }
}