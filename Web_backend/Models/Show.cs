﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_backend.Models
{
    public class Show
    {
        public class Genre
        {
            public List<List_Genre> genre { get; set; }
            

        }
        public class Studio
        {
            public List<List_Studio> studio { get; set; }
        }
        public class News
        {
            public List<List_News> news { get; set; }
        }

    }
    public class List_Genre
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
    public class List_Studio
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
    public class List_News
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}