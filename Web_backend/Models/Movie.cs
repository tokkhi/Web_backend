﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web_backend.Models
{
    public class Movie
    {
        public List<List_Movie> movie { get; set; }
        

    }
    public class List_Movie
    {
        public string ID_Movie { get; set; }
        public string Name { get; set; }
        public string date { get; set; }
        public bool status { get; set; }
    }
    public class Add_Movie
    {
        public List<list_DD> Caster { get; set; }
        public List<list_DD> Rate { get; set; }
        public List<list_DD> Director { get; set; }
        public List<list_DD> Genre { get; set; }
        public List<list_DD> Studio { get; set; }
        public List<list_DD> S_Caster { get; set; }
        public List<list_DD> S_Director { get; set; }
    }
    public class Edit_Movie
    {

        public List<list_DD> Caster { get; set; }
        public List<list_DD> S_Caster { get; set; }
        public List<list_DD> Rate { get; set; }
        public List<list_DD> Directer { get; set; }
        public List<list_DD> S_Director { get; set; }
        public List<list_DD> Genre { get; set; }
        public List<list_DD> Studio { get; set; }
    }
    public class list_DD { 
        public string id { get; set; }
        public string Name { get; set; }
    }
}